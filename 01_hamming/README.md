# Hamming

Ermittele die Hamming-Differenz zwischen zwei DNA-Strängen.

Eine Mutation ist ein Fehler, der bei der Produktion oder beim Kopieren einer Nukleinsäure, insbesondere der DNA, auftritt. Da Nukleinsäuren für die Zellfunktionen lebenswichtig sind, neigen Mutationen dazu, einen Dominoeffekt in der gesamten Zelle zu verursachen. Obwohl Mutationen rein theoretisch Fehler sind, kann eine sehr begrenzte Anzahl von Mutationen die Zelle mit einem positiven Attribut ausstatten. Tatsächlich sind die Makroeffekte der Evolution auf das kumulierte Ergebnis vorteilhafter mikroskopischer Mutationen über viele Generationen zurückzuführen.

Die simpelste und häufigste Art der Nukleinsäure-Mutation ist eine Punktmutation, die eine Base an einem einzigen Nukleotid durch eine andere ersetzt.

Durch das Zählen der Anzahl der Unterschiede zwischen zwei homologen DNA-Strängen aus verschiedenen Genomen eines gemeinsamen Vorfahren erhalten wir einen Messwert bezüglich der minimalen Anzahl von Punktmutationen, die auf dem evolutionären Pfad der beiden Stränge in Relation zueinander stattgefunden haben.

Dieser Wert wird als'Hamming-Distanz' bezeichnet.

Die Hamming-Distanz wird ermittelt indem man zwei DNA-Stränge vergleicht und zählt, wie viele Nukleotide sich zwischen den beiden homologen Strängen unterscheiden:

    GAGCCTACTAACGGGAT
    CATCGTAATGACGGCCT
    ^ ^ ^  ^ ^    ^^

Die Hamming-Distanz zwischen diesen DNA-Strängen ist 7.

## Hinweise zur Umsetzung

Die Hamming-Distanz ist nur für Sequenzen gleicher Länge definiert, daher sollte ein Versuch, sie zwischen Sequenzen unterschiedlicher Länge zu berechnen, nicht funktionieren.
Falls die zwei zu vergleichenden DNA-Stränge von ungleicher Länge sind, solltest du einen `error` ausgeben!

## Durchführung der Tests

Um die Tests auszuführen, gib den Befehl ``go test`` oder `go test -v` innerhalb des Übungsverzeichnisses ein, z.B.:

```plain
linus@DESKTOP-QL755PU MINGW64 ~/go/src/gitlab.com/fws-ag/05_treffen/übungen/01_hamming
$ go test -v
# gitlab.com/fws-ag/05_treffen/übungen/01_hamming [gitlab.com/fws-ag/05_treffen/übungen/01_hamming.test]
.\hamming.go:9:1: missing return at end of function
FAIL    gitlab.com/fws-ag/05_treffen/übungen/01_hamming [build failed]
```