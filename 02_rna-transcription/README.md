# RNA Transkription

Ermittele das RNA-Komplement eines DNA-Stranges (per RNA-Transkription).

Sowohl DNA- als auch RNA-Stränge sind eine Sequenz von Nukleotiden.

Die vier Nukleotide in der DNA sind Adenin (**A**), Cytosin (**C**), Guanin (**G**) und Thymin (**T**).

Die vier Nukleotide in der RNA sind Adenin (**A**), Cytosin (**C**), Guanin (**G**) und Uracil (**U**).

Bei einem DNA-Strang wird sein übertragener RNA-Strang gebildet, indem jedes Nukleotid durch sein Komplement ersetzt wird:

* `G` -> `C`
* `C` -> `G`
* `T` -> `A`
* `A` -> `U`

## Durchführung der Tests

Um die Tests auszuführen, gib den Befehl ``go test`` oder `go test -v` innerhalb des Übungsverzeichnisses ein, z.B.:

```plain
linus@DESKTOP-QL755PU MINGW64 ~/go/src/gitlab.com/fws-ag/05_treffen/übungen/01_hamming
$ go test -v
# gitlab.com/fws-ag/05_treffen/übungen/01_hamming [gitlab.com/fws-ag/05_treffen/übungen/01_hamming.test]
.\hamming.go:9:1: missing return at end of function
FAIL    gitlab.com/fws-ag/05_treffen/übungen/01_hamming [build failed]
```