// Package proverb should have a package comment that summarizes what it's about.
// https://golang.org/doc/effective_go.html#commentary
package proverb

import "fmt"

// Proverb generates a rhyme from a slice of proverbs
func Proverb(rhyme []string) []string {

	if len(rhyme) == 0 {
		return []string{}
	}

	const (
		schemaZeile   = "For want of a %s the %s was lost."
		schemaSchluss = "And all for the want of a %s."
	)

	res := make([]string, len(rhyme))
	for i := 0; i < len(rhyme)-1; i++ {
		res = append(res, fmt.Sprintf(schemaZeile, rhyme[i], rhyme[i+1]))
	}
	res = append(res, fmt.Sprintf(schemaSchluss, rhyme[0]))

	return res
}

// Alternative:

// Proverb generates a rhyme from a slice of proverbs
// func Proverb(rhyme []string) []string {
// 	res := make([]string, len(rhyme))

// 	const (
// 		schemaZeile   = "For want of a %s the %s was lost."
// 		schemaSchluss = "And all for the want of a %s."
// 	)

// 	for i, l := range rhyme {
// 		if i < len(rhyme)-1 {
// 			res[i] = fmt.Sprintf(schemaZeile, l, rhyme[i+1])
// 		} else {
// 			res[i] = fmt.Sprintf(schemaSchluss, rhyme[0])
// 		}
// 	}
// 	return res
// }
