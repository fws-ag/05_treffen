# Nucleotide Count

Ausgehend von einem einzelsträngigen DNA-Strang soll berechnet werden, wie oft jedes Nukleotid in dem Strang vorkommt.

Die genetische Sprache jedes Lebewesens auf dem Planeten ist die DNA.
Die DNA ist ein großes Molekül, das aus einer extrem langen Sequenz von Einzelelementen, den Nukleotiden, aufgebaut ist.
4 Typen existieren in der DNA. Diese unterscheiden sich nur geringfügig und können als folgende Symbole dargestellt werden:'A' für Adenin,'C' für Cytosin,'G' für Guanin und'T' Thymin.

Hier ist eine Analogie:

- Zweige sind für Vogelnester wie
- Nukleotide für DNA,
- Legosteine für Lego-Häuser,
- Worte für Sätze,
- ...

## Implementation

Du solltest einen eigenen Typ 'DNA' mit der Funktion' Counts' definieren, der zwei Werte ausgibt:

- eine Häufigkeitszahl für den gegebenen DNA-Strang
- einen Fehler (wenn es ungültige Nukleotide gibt)

Was ist ein guter Typ für einen DNA-Strang?

Welche sind die besten Go-Typen, um die Ausgabewerte darzustellen?

Wirf einen Blick auf die Testfälle, um einen Hinweis darauf zu erhalten, was die möglichen Eingaben sein könnten.

## Anmerkung zu den Tests

Um die Tests auszuführen benutze `go test` innerhalb des Aufgabenverzeichnisses (in diesem Fall 04_nucleotide-count).
