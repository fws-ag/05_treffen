package main

import (
	"fmt"
	"time"
)

func fib(n uint) uint {
	if n < 2 {
		return n
	}

	return fib(n-1) + fib(n-2)
}

func memoize(f func(uint) uint) func(uint) uint {
	cache := make(map[uint]uint)

	return func(n uint) uint {
		if cache[n] == 0 {
			cache[n] = f(n)
		}
		return cache[n]
	}
}

func memoizeV2(f func(uint) uint, cache map[uint]uint) func(uint) uint {
	zwischenschicht := func(n uint) uint {
		if _, exists := cache[n]; exists {
			return cache[n]
		}
		return f(n)
	}

	return func(n uint) uint {
		for cache[n] == 0 {
			cache[n] = zwischenschicht(n-1) + zwischenschicht(n-2)
		}
		return cache[n]
	}
}

func main() {
	//fmt.Println(fib(42))

	//	memoMap := make(map[uint]uint, 0)

	memoizedFibV1 := memoize(fib)

	start := time.Now()
	for i := uint(0); i <= 42; i++ {
		fmt.Println(memoizedFibV1(i))
	}

	timeNotCached := time.Since(start)

	start = time.Now()
	for i := uint(0); i <= 42; i++ {
		fmt.Println(memoizedFibV1(i))
	}

	timeCached := time.Since(start)

	// V2
	cache := make(map[uint]uint)
	cache[0] = 0
	cache[1] = 1

	memoizedFibV2 := memoizeV2(fib, cache)

	start = time.Now()
	for i := uint(1); i <= 42; i++ {
		fmt.Println(memoizedFibV2(i))
	}

	timeV2 := time.Since(start)

	fmt.Println("----")
	fmt.Println("TimeV1 not chached: ", timeNotCached)
	fmt.Println("TimeV1 cached: ", timeCached)
	fmt.Println("memoizedFibV2: ", timeV2)
	// fmt.Println(fibonacciLoop(42))
}

// Ein Beispiel ohne Rekursion - schneller, aber nicht so schön :P
func fibonacciLoop(n int) int {
	fn := make(map[int]int)

	for i := 0; i <= n; i++ {

		var f int

		if i <= 2 {
			f = 1
		} else {
			f = fn[i-1] + fn[i-2]
		}

		fn[i] = f
	}
	return fn[n]
}

// func fibDyn(n uint, cache map[uint]uint) uint {
// 	var val uint

// 	switch n {
// 	case 0 | 1:
// 		return 1
// 	case n:
// 		if val, ok := memo[n]; ok {
// 			return memo[n]
// 		} else {
// 			val = fibDyn(n-1, memo) + fibDyn(n-2, memo)
// 			memo[n] = val
// 		}
// 	}

// 	return val
// }
