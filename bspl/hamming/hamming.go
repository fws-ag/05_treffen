// Package hamming provides functionality to compute the hamming distance between two DNA strands
package hamming

import "errors"

// Distance computes the hamming distance between to DNA strands
func Distance(a, b string) (int, error) {

	if len(a) != len(b) {
		return 0, errors.New("input strings are of unequal length")
	}

	var distance int
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			distance++
		}
	}
	return distance, nil
}
