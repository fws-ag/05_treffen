package main

import "fmt"

func main() {

	fmt.Println(proverb([]string{"nail", "shoe", "horse"}))

}

func proverb(rhyme []string) []string {

	if len(rhyme) == 0 {
		return []string{}
	}

	const (
		schemaZeile   = "For want of a %s the %s was lost."
		schemaSchluss = "And all for the want of a %s."
	)

	res := make([]string, len(rhyme))
	for i := 0; i < len(rhyme)-1; i++ {
		res = append(res, fmt.Sprintf(schemaZeile, rhyme[i], rhyme[i+1]))
	}
	res = append(res, fmt.Sprintf(schemaSchluss, rhyme[0]))

	return res
}
