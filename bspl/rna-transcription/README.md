# RNA Transcription

Ermittele das RNA-Komplement eines DNA-Stranges (per RNA-Transkription).

Sowohl DNA- als auch RNA-Stränge sind eine Sequenz von Nukleotiden.

Die vier Nukleotide in der DNA sind Adenin (**A**), Cytosin (**C**), Guanin (**G**) und Thymin (**T**).

Die vier Nukleotide in der RNA sind Adenin (**A**), Cytosin (**C**), Guanin (**G**) und Uracil (**U**).

Bei einem DNA-Strang wird sein übertragener RNA-Strang gebildet, indem jedes Nukleotid durch sein Komplement ersetzt wird:

* `G` -> `C`
* `C` -> `G`
* `T` -> `A`
* `A` -> `U`

## Running the tests

To run the tests run the command `go test` from within the exercise directory.

If the test suite contains benchmarks, you can run these with the `--bench` and `--benchmem`
flags:

    go test -v --bench . --benchmem

Keep in mind that each reviewer will run benchmarks on a different machine, with
different specs, so the results from these benchmark tests may vary.

## Further information

For more detailed information about the Go track, including how to get help if
you're having trouble, please visit the exercism.io [Go language page](http://exercism.io/languages/go/resources).

## Source

Hyperphysics [http://hyperphysics.phy-astr.gsu.edu/hbase/Organic/transcription.html](http://hyperphysics.phy-astr.gsu.edu/hbase/Organic/transcription.html)

## Submitting Incomplete Solutions
It's possible to submit an incomplete solution so you can see how others have completed the exercise.
