package strand

// ToRNA converts a DNA strand to its RNA counterpart
func ToRNA(dna string) string {
	translationTable := map[string]string{
		"A": "U",
		"C": "G",
		"G": "C",
		"T": "A",
	}

	var res string

	for _, Buchstabe := range dna {
		res += translationTable[string(Buchstabe)]
	}

	return res
}

// Naive Alternative:

// ToRNA converts a DNA strand to its RNA counterpart
// func ToRNA(dna string) string {

// 	var res string

// 	for _, r := range dna {
// 		switch {
// 		case string(r) == "A":
// 			res += "U"
// 		case string(r) == "C":
// 			res += "G"
// 		case string(r) == "G":
// 			res += "C"
// 		case string(r) == "T":
// 			res += "A"
// 		}
// 	}

// 	return res
// }

// Oder:

// func ToRNA(dna string) string {
// 	dna = strings.Replace(dna, "A", "u", -1)
// 	dna = strings.Replace(dna, "T", "a", -1)
// 	dna = strings.Replace(dna, "C", "g", -1)
// 	dna = strings.Replace(dna, "G", "c", -1)
// 	return strings.ToUpper(dna)
// }
