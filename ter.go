package main

import "fmt"

func findMin(arr []int) int {

	var min int

	for i, e := range arr {
		if i == 0 || e < min {
			min = e
		}
	}

	return min
}

func main() {
	fmt.Println(findMin([]int{612, 215, 25, 51, 67}))
}
